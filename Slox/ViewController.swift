//  Copyright 2014 Alexander Orlov <alexander.orlov@loxal.net>. All rights reserved.

import UIKit
import LocalAuthentication

class ViewController: UIViewController, FBLoginViewDelegate {
    
    @IBOutlet var fbLoginView : FBLoginView!
    @IBOutlet var email: UITextField?
    @IBOutlet var password: UITextField?
    @IBOutlet var signupStatus: UILabel?
    @IBOutlet var signupDescription: UITextView?
    
    struct Auth {
        static var accessToken : NSString?
    }
    
    @IBAction
    func signup(sender : AnyObject) {
        NSLog("\(email?.restorationIdentifier): \(email?.text)")
        NSLog("\(password?.restorationIdentifier): \(password?.text)")
        
        createNewCustomerAccount()
    }
    
    @IBAction
    func authenticateTouch(sender : AnyObject) {
        let touchIDContext = LAContext()
        let reasonString = "Login"
        var touchIDError : NSError?
        
        if touchIDContext.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &touchIDError) {
            touchIDContext.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: {
                (success: Bool, error: NSError?) -> Void in
                if success {
                    NSLog("Success")
                } else {
                    NSLog("Failure")
                }
            })
        } else {
            NSLog("Touch ID Failure")
        }
    }
    
    @IBAction
    func authenticate(sender : AnyObject) {
        NSLog("\(email?.restorationIdentifier): \(email?.text)")
        NSLog("\(password?.restorationIdentifier): \(password?.text)")
        
        let url = NSURL(string: "http://yaas-test.apigee.net/test/customer/v4/login")
        let request = NSMutableURLRequest(URL: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(Auth.accessToken!)", forHTTPHeaderField: "Authorization")

        NSLog("Authorization: Bearer \(Auth.accessToken!)")
        request.HTTPMethod = "POST"
        
        let payload = "{\"email\": \"\(email!.text)\", \"password\": \"\(password!.text)\"}"
        NSLog(payload)
        let payloadData = (payload as String).dataUsingEncoding(NSUTF8StringEncoding)
        request.HTTPBody = payloadData
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) in
            // fix this SAP bug
            NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
            var errror: NSError?
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &errror) as Dictionary<String, AnyObject>
            
            if let e = error {
                NSLog("JSON Error \(e.description)")
            } else {
                NSLog("No JSON parsing error")
            }
            
            let accessToken: String? = json["accessToken"] as NSString?
            if(accessToken == nil){
                let errorMessage: String? = json["message"]?.description
                self.signupStatus!.text = "Error: \(errorMessage)"
                self.signupDescription!.text = "\(json)"
            } else {
                self.signupStatus!.text = "Successfully Logged In"
                self.signupDescription!.text = "AccessToken: \(accessToken!)"
                Auth.accessToken = accessToken!
                self.showProfile(accessToken!)
            }
        }
    }
    
    func createNewCustomerAccount() {
        let url = NSURL(string: "http://yaas-test.apigee.net/test/customer/v4/signup")
        let request = NSMutableURLRequest(URL: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.addValue("Bearer \(Auth.accessToken!)", forHTTPHeaderField: "Authorization")

        
        let payload = "{\"email\": \"\(email!.text)\", \"password\": \"\(password!.text)\"}"
        NSLog(payload)
        let payloadData = (payload as String).dataUsingEncoding(NSUTF8StringEncoding)
        request.HTTPBody = payloadData
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) in
            // fix this SAP bug
            NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
            var errror: NSError?
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &errror) as Dictionary<String, AnyObject>
            
            if let e = error {
                NSLog("JSON Error \(e.description)")
            } else {
                NSLog("No JSON parsing error")
            }
            
            let accountId: String? = json["_id"] as NSString?
            if(accountId == nil){
                let errorMessage: String? = json["message"]?.description
                self.signupStatus!.text = "Error: \(errorMessage)"
                self.signupDescription!.text = "\(json)"
            } else {
                self.signupStatus!.text = "Account successfully created!"
                self.signupDescription!.text = "Account created: \(accountId)"
            }
        }
    }
    
    func showProfile(hybrisAcessToken: String?) {
        if let hat = hybrisAcessToken {
            NSLog("BLUB: \(hybrisAcessToken!)")
            let url = NSURL(string: "http://yaas-test.apigee.net/test/customer/v4/me")
            let request = NSMutableURLRequest(URL: url)
            
            request.addValue("Bearer \(hybrisAcessToken!)", forHTTPHeaderField: "Authorization")
            request.HTTPMethod = "GET"
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response, data, error) in
                NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
                var errror: NSError?
                let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &errror) as Dictionary<String, AnyObject>
                NSLog(json["customerNumber"] as NSString)
                let customerNumber: String =            json["customerNumber"] as NSString
                self.signupDescription!.text = "Customer Number: \(customerNumber)"
            }
        }
    }
    
    func getAnonymousAccess() {

        let projectId = "5ltw0mok71ni" // todo make a config variable
        let url = NSURL(string: "http://yaas-test.apigee.net/test/account/v1/auth/anonymous/login?hybris-tenant=\(projectId)")
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response, data, error) in
                NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
                var errror: NSError?
                if let httpResponse = response as? NSHTTPURLResponse {
                    if (httpResponse.statusCode == 200) {
                        let responseLocation = httpResponse.allHeaderFields["location"] as NSString
                        let authtoken: AnyObject = responseLocation.componentsSeparatedByString("&access_token=").reverse()[0].componentsSeparatedByString("&")[0]
                
                        NSLog(authtoken as NSString)
                        self.signupDescription!.text = "Auth token: \(authtoken)"
                        Auth.accessToken = (authtoken as NSString)
                        
                        self.showProfile(Auth.accessToken as? String)
                    }
                    else {
                        NSLog("error retrieving auth token: code \(httpResponse.statusCode)")
                        self.signupDescription!.text = "error retrieving auth token: code \(httpResponse.statusCode)"
                    }
                }
                
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("Do any additional setup after loading the view, typically from a nib.")
        self.signupDescription!.text = ""
        
        self.fbLoginView.delegate = self
        
        getAnonymousAccess()
    }
    
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        println("User Logged In")
    }
    
    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
        let session: FBSession? = FBSession.activeSession()
        NSLog("\(session?.accessTokenData)")
        
        
        println("User: \(user)")
        println("User ID: \(user.objectID)")
        println("User Name: \(user.name)")
        
        let url = NSURL(string: "http://yaas-test.apigee.net/test/customer/v4/login/facebook")
        let request = NSMutableURLRequest(URL: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(Auth.accessToken!)", forHTTPHeaderField: "Authorization")
        request.HTTPMethod = "POST"
        
        let accessToken = session?.accessTokenData
        
        NSLog("====== \(accessToken!)=")
        let payload = "{\"accessToken\": \"\(accessToken!)\"}"
        NSLog(payload)
        let payloadData = (payload as String).dataUsingEncoding(NSUTF8StringEncoding)
        request.HTTPBody = payloadData
        
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) in
            NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
            var errror: NSError?
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &errror) as Dictionary<String, AnyObject>
            
            let hybrisAccessToken: String? = json["accessToken"] as NSString?
            NSLog(hybrisAccessToken!)
            self.showProfile(hybrisAccessToken)
            
        }
    }
    
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        println("User Logged Out")
        self.signupDescription!.text = ""
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        println("Error: \(handleError.localizedDescription)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        NSLog("Dispose of any resources that can be recreated.")
    }
}